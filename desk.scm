(use-modules
 (gnu home)
 (gnu home services shells)
 (gnu home services shepherd)
 (gnu packages)
 (gnu packages aidc)
 (gnu packages aspell)
 (gnu packages audio)
 (gnu packages authentication)
 (gnu packages base)
 (gnu packages calendar)
 (gnu packages cdrom)
 (gnu packages chromium)
 (gnu packages commencement)
 (gnu packages databases)
 (gnu packages dns)
 (gnu packages ebook)
 (gnu packages emacs)
 (gnu packages emacs-xyz)
 (gnu packages engineering)
 (gnu packages fcitx)
 (gnu packages fonts)
 (gnu packages freedesktop)
 (gnu packages gimp)
 (gnu packages gl)
 (gnu packages gnome)
 (gnu packages graphviz)
 (gnu packages gv)
 (gnu packages ibus)
 (gnu packages image)
 (gnu packages image-viewers)
 (gnu packages jami)
 (gnu packages libreoffice)
 (gnu packages libusb)
 (gnu packages markup)
 (gnu packages maths)
 (gnu packages music)
 (gnu packages cups)
 (gnu packages password-utils)
 (gnu packages patchutils)
 (gnu packages pdf)
 (gnu packages pulseaudio)
 (gnu packages rsync)
 (gnu packages rust-apps)
 (gnu packages security-token)
 (gnu packages sequoia)
 (gnu packages sqlite)
 (gnu packages suckless)
 (gnu packages terminals)
 (gnu packages video)
 (gnu packages wm)
 (gnu packages xdisorg)
 (gnu packages xfce)
 (gnu packages xorg)
 (gnu services)
 (guix gexp)
 (juix deploy guile-exec)
 (nongnu packages fonts)
 (nongnu packages mozilla))

(home-environment
 (packages
  (append

   (load "packages/admin.scm")
   (load "packages/containers.scm")
   (load "packages/development.scm")
   (load "packages/mail.scm")
   (load "packages/network.scm")
   (load "packages/shells.scm")

   (list

    alsa-utils
    candle
    cups
    emacs-no-x-toolkit
    emacs-dired-preview
    emacs-display-wttr
    emacs-exwm
    emacs-geiser
    emacs-geiser-guile
    emacs-gnuplot
    emacs-guix
    emacs-jabber
    emacs-nerd-icons
    evince
    fcitx
    fcitx-configtool
    fcitx-qt5
    feh
    firefox
    font-adobe-source-han-sans
    font-culmus
    font-fira-go
    font-google-noto
    font-google-noto-emoji
    font-microsoft-web-core-fonts
    font-sil-ezra
    font-vazir
    font-wqy-microhei
    font-wqy-zenhei
    gimp
    gmtp
    gnome-disk-utility
    gnuplot
    graphviz
    guile-exec/juix
    gv
    gvfs
    handbrake
    hebcal
    hicolor-icon-theme
    ibus
    ibus-libpinyin
    ispell
    isync
    jmtpfs
    (list knot "tools")
    (list  ldns "drill")
    libcdio
    libdvdcss
    libevdev
    libfido2
    libiconv
    libmtp
    libreoffice
    markdown
    mesa-utils
    mplayer
    mu
    mupdf
    netcat
    notmuch
    pamtester
    pass-otp
    password-store
    pavucontrol
    pinentry-emacs
    pius
    postgresql
    rsync
    ripgrep
    simple-scan
    slock
    sqlite                              ; for Emacs
    sx
    thunar
    udiskie
    vlc
    xclip
    xdg-utils
    xdot
    xev
    xeyes
    xf86-input-evdev
    xf86-input-libinput
    xinit
    xinput
    xlockmore
    xmodmap
    xset
    xterm
    xorg-server
    xrandr
    zbar)))

 (services
  (list

   (load "service/bash.scm")
   (load "service/ibus.scm")
   #; (load "service/redshift.scm"))))

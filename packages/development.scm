(use-modules
 (gnu packages guile)
 (gnu packages haskell)
 (gnu packages perl)
 (gnu packages version-control))

(list

 binutils
 (list git "send-email")
 ghc
 guile-next
 meld
 perl)

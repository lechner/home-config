(use-modules
 (gnu packages bash)
 (gnu packages shells))

(list

 bash
 bash-completion
 byobu
 gash
 screen
 scsh)

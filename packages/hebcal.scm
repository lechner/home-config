(use-modules (gnu packages golang)
             (guix build-system go)
             (guix gexp)
             (guix git-download)
             ((guix licenses) #:prefix license:)
             (guix packages))

(define-public go-github-com-nathan-osman-go-sunrise
  (let ((commit "c8f9f1eb869135f07378e7e3c5ec7a005f806c73")
        (revision "0"))
    (package
      (name "go-github-com-nathan-osman-go-sunrise")
      (version (git-version "1.1.0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/nathan-osman/go-sunrise")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "017zwzx05r5spxcs07dp6bnh7waknzsd819k7aqd8kr819v3x9in"))))
      (build-system go-build-system)
      (arguments
       '(#:import-path "github.com/nathan-osman/go-sunrise"))
      (home-page "https://github.com/nathan-osman/go-sunrise")
      (synopsis "Calculates the sunrise and sunset times for a given location")
      (description
       "To calculate the sunrise and sunset times, you will need the date of your
observation and the geographical coordinates in latitude and longitude for the
location.")
      (license license:expat))))

(define-public go-github-com-hebcal-gematriya
  (let ((commit "fe3043f73e415eb82727701d10f2fb40f87675e9")
        (revision "0"))
    (package
      (name "go-github-com-hebcal-gematriya")
      (version (git-version "1.0.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/hebcal/gematriya")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "0xmnb2i80dy380yv8c4pd04bbyqgbc7c40p8hz1vqj2lhbm6jabf"))))
      (build-system go-build-system)
      (arguments
       '(#:import-path "github.com/hebcal/gematriya"))
      (home-page "https://github.com/hebcal/gematriya")
      (synopsis "Provides a system of writing numbers as Hebrew letters")
      (description
       "Prints numbers as Hebrew letters.  Numerical values are represented using
letters of the Hebrew alef-bet (alphabet).")
      (license license:bsd-2))))

(define-public go-github-com-hebcal-hebcal-go
  (let ((commit "d42e881860cfc9e8249fc79f268091c3c4d36b0d")
        (revision "0"))
    (package
      (name "go-github-com-hebcal-hebcal-go")
      (version (git-version "0.9.11" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/hebcal/hebcal-go")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "1m9akb8pwxchpaci05gambshrzw626gsrfhl25f36vjl7mq5292n"))))
      (build-system go-build-system)
      (arguments
       (list
        #:import-path "github.com/hebcal/hebcal-go"
        ;; Source-only package
        #:tests? #f
        #:phases
        #~(modify-phases %standard-phases
            ;; Source-only package
            (delete 'build))))
      (native-inputs
       (list go-github-com-stretchr-testify))
      (propagated-inputs
       (list go-github-com-hebcal-gematriya
             go-github-com-nathan-osman-go-sunrise))
      (home-page "https://github.com/hebcal/hebcal-go")
      (synopsis "Library module for Hebcal, a perpetual Jewish Calendar")
      (description
       "This library converts between Hebrew and Gregorian dates, and generates lists
of Jewish holidays for any year (past, present or future). Shabbat and holiday candle
lighting and havdalah times are approximated based on location.

Torah readings (Parashat HaShavua), Daf Yomi, and counting of the Omer can also be
specified. Hebcal also includes algorithms to calculate yahrzeits, birthdays and
anniversaries.")
      (license license:gpl2+))))

(define-public hebcal
  (let ((commit "2384bb88dc1a41a4a5ae57a29fb58b2dd49a475d")
        (revision "0"))
    (package
      (name "hebcal")
      (version (git-version "5.3.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/hebcal/hebcal")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "12rv3b51jb7wcjwmmizz9jkw7gh37yklys4xncvpzgxdkkfgmmjx"))))
      (build-system go-build-system)
      (arguments
       '(#:import-path "github.com/hebcal/hebcal"))
      (native-inputs
       (list go-github-com-hebcal-hebcal-go
             go-github-com-pborman-getopt))
      (synopsis "A perpetual Jewish Calendar")
      (description "Hebcal is a program which prints out the days in the Jewish calendar
for a given Gregorian year.")
      (home-page "https://github.com/hebcal/hebcal")
      (license license:gpl2+))))

(list
 hebcal)


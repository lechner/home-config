(use-modules
 (gnu packages admin)
 (gnu packages dns)
 (gnu packages networking))

(list

 iperf
 (list knot "tools")
 netcat
 whois)

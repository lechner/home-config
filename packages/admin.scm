(use-modules
 (gnu packages admin)
 (gnu packages backup)
 (gnu packages check)
 (gnu packages compression)
 (gnu packages curl)
 (gnu packages disk)
 (gnu packages file)
 (gnu packages gnupg)
 (gnu packages kerberos)
 (gnu packages linux)
 (gnu packages python-xyz)
 (gnu packages screen)
 (gnu packages texinfo)
 (gnu packages tls)
 (gnu packages version-control)
 (gnu packages web-browsers)
 (gnu packages wget)
 (gnu packages zile))

(list
 borg
 certbot
 curl
 dmidecode
 dosfstools
 file
 git
 gnupg
 gptfdisk
 heimdal
 libfaketime
 lvm2
 lynx
 mdadm
 openssl
 parted
 python-diceware
 texinfo
 unzip
 wget
 zile)

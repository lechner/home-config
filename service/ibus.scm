(use-modules
 (gnu home services))

(simple-service
 'im-env-vars home-environment-variables-service-type
 '(("GTK_IM_MODULE" . "ibus")
   ("QT_IM_MODULE" . "ibus")
   ("XMODIFIERS" . "@im=ibus")
   ;; TODO: Are these still required?  If yes, try to get rid of them.
   ("GUIX_GTK2_IM_MODULE_FILE"
    . "$HOME/.guix-home/profile/lib/gtk-2.0/2.10.0/immodules-gtk2.cache")
   ("GUIX_GTK3_IM_MODULE_FILE"
    . "$HOME/.guix-home/profile/lib/gtk-3.0/3.0.0/immodules-gtk3.cache")))

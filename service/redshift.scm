(use-modules
 (gnu home services desktop))

(service home-redshift-service-type
         (home-redshift-configuration
          (location-provider 'manual)
          ;; Fremont, Calif.
          (latitude 37.5)
          (longitude -122.0)))

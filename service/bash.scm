(use-modules
 (gnu home services shells))

(service home-bash-service-type
         (home-bash-configuration
          (aliases '(("grep" . "grep --color=auto")
                     ("ll" . "ls -l")
                     ("ls" . "ls -p --color=auto")))
          (bashrc (list
                   (local-file
                    "../bash/.bashrc"
                    "bashrc")))
          (bash-profile (list
                         (local-file
                          "../bash/.bash_profile"
                          "bash_profile")))))

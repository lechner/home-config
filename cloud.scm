(use-modules
 (gnu home)
 (gnu packages databases)
 (gnu packages dns)
 (gnu packages emacs)
 (gnu packages emacs-xyz)
 (gnu packages irc)
 (gnu packages ncurses)
 (gnu packages sqlite)
 (gnu packages terminals)
 (gnu services)
 (guix gexp))

(home-environment
 (packages
  (append

   (load "packages/admin.scm")
   (load "packages/mail.scm")
   (load "packages/network.scm")
   (load "packages/shells.scm")

   (list

    emacs-no-x
    emacs-vterm
    foot
    ldns
    mariadb
    ncurses
    sqlite ; for Emacs
    weechat)))

 (services
  (list

   (load "service/bash.scm"))))
